$(document).ready(function () {

  //вызов функции
  adaptive();

  //вызов функции при ресайзе окна браузера
  $(window).resize(function () {
    adaptive();
  });

  // перемещение первой колонки вниз при разрешении <= 320px
  function adaptive() {
    var w = $(window).width();
    console.log(w);
    if (document.body.clientWidth <= '320') {
      $(".gallery__center").after($(".gallery__left"));
    } else {
      $(".gallery__center").before($(".gallery__left"));
    }
  };
});
